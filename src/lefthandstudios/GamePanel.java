package lefthandstudios;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

public class GamePanel extends JPanel implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8689310483557860437L;

	public GamePanel(BorderLayout borderLayout) {
		super(borderLayout);
	}

	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()){
		case "QUIT":
			System.exit(0);
		case "NEW":
			for (Component comp : getComponents()) {
				if(comp.getClass() == BigBoard.class)
					((BigBoard) comp).clearBoard();
				//Alternatively:
				//remove(comp);
				//add(new BigBoard(1), BorderLayout.CENTER);
				repaint();
			}
			break;
		}
	}
}
package lefthandstudios;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

public class BigBoard extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int size = 500;
	public static int third = size/3;
	public static int ninth = size/9;
	public Board[] board = new Board[9];
	public Square[] winsBoard = new Square[9];
	public Square winner = new Square();
	private int buffer = 0;

	public BufferedImage bimg = new BufferedImage(9, 9, BufferedImage.TYPE_3BYTE_BGR);
	public BufferedImage lines = new BufferedImage(size, size, BufferedImage.BITMASK);
	//	public BufferedImage X = new BufferedImage(third, third, BufferedImage.BITMASK);
	//	public BufferedImage O = new BufferedImage(111, 111, BufferedImage.BITMASK);
	//	public BufferedImage BIGX = new BufferedImage(333, 333, BufferedImage.BITMASK);
	//	public BufferedImage BIGO = new BufferedImage(333, 333, BufferedImage.BITMASK);
	//public pane pane = new pane();
	public State last = State.X;

	/** 
	 * Creates a new board
	 * @param inSize: the size of the board to create
	 */
	public BigBoard(int inSize){
		size = inSize;
		third = size/3;
		ninth = size/9;

		//add listener for mouse events
		addMouseListener(new MouseHandler());

		//Initialize all of the subboards and board for tracking the winners
		for( int i = 0; i < 9; i++)
			board[i] = new Board();
		for (int big = 0; big < winsBoard.length; big++)
			winsBoard[big] = new Square();

		//Load in the gameboard images
		Square.init();

		//onResize() can't be called yet, there isn't usually a containing frame at this point
	}

	/**
	 * The proper way to clear everything out and start again
	 */
	public void clearBoard(){
		System.out.println("Clearing Board");

		//No winner
		winner.state = State.BLANK;

		//Reinitialize boards
		for( int i = 0; i < 9; i++)
			board[i] = new Board();
		
		//Then push through logic for "clearing out the pipes" and redraw the new empty board
		checkWinner();
		repaint();
	}

	/**
	 * Called whenever the board is resized, so that everything looks nice and not jaggy
	 */
	public void onResize(){
		//change the size variables to do logic properly
		size = getHeight() < getWidth() ? getHeight() : getWidth();
		third = size/3;
		ninth = size/9;

		//resize the horizontal buffer for centering the board in the window
		buffer = getWidth() > getHeight()? (getWidth()-getHeight())/2 : 0;

		//fix errors from window being too small
		if(size < 0){
			size = 1;
			third = size/3;
			ninth = size/9;
		}
		
		//Reload and rebuffer x's and o's with fancy smoothing
		Square.resizeImgs();
		
		//redraw lines manually for a clean look, not strange from scaling
		lines = new BufferedImage(size, size, BufferedImage.BITMASK);
		Graphics g1 = lines.createGraphics();
		g1.setColor(Color.white);
		g1.drawLine(1*ninth, 0, 1*ninth, size);
		g1.drawLine(2*ninth, 0, 2*ninth, size);
		g1.drawLine(4*ninth, 0, 4*ninth, size);
		g1.drawLine(5*ninth, 0, 5*ninth, size);
		g1.drawLine(7*ninth, 0, 7*ninth, size);
		g1.drawLine(8*ninth, 0, 8*ninth, size);
		g1.drawLine(0, 1*ninth, size, 1*ninth);
		g1.drawLine(0, 2*ninth, size, 2*ninth);
		g1.drawLine(0, 4*ninth, size, 4*ninth);
		g1.drawLine(0, 5*ninth, size, 5*ninth);
		g1.drawLine(0, 7*ninth, size, 7*ninth);
		g1.drawLine(0, 8*ninth, size, 8*ninth);
		g1.fillRect(third-3, 0, 6, size);
		g1.fillRect(2*third -3, 0, 6, size);
		g1.fillRect(0, third-3, size, 6);
		g1.fillRect(0, 2*third - 3, size, 6);
	}

	/**
	 * The proper way to determine if the game has been won.  
	 * Checks each individual board then the overview board.
	 */
	public void checkWinner(){
		//	winner.state = State.BLANK;
		for (int i = 0; i < board.length; i++) {
			winsBoard[i].state = board[i].betterCheckWinner();
		}

		for (int i = 0; i < winsBoard.length; i += 3) {
			if ( winsBoard[i].state.equals(State.X) || winsBoard[i].state.equals(State.O) ) { // horizontal check
				if (winsBoard[i].equals(winsBoard[i+1]) && winsBoard[i+1].equals(winsBoard[i+2])) {
					if(winsBoard[i].state.equals(State.X))
						winner.state = State.X;
					if(winsBoard[i].state.equals(State.O))
						winner.state = State.O;
				}
			}
		}

		for (int i = 0; i < 3; i++) {
			if ( winsBoard[i].state.equals(State.X) || winsBoard[i].state.equals(State.O) ) { // vertical check
				if (winsBoard[i].equals(winsBoard[i+3]) && winsBoard[i+3].equals(winsBoard[i+6])) {
					if(winsBoard[i].state.equals(State.X))
						winner.state = State.X;
					if(winsBoard[i].state.equals(State.O))
						winner.state = State.O;
				}
			}
		}

		if ( winsBoard[0].state.equals(State.X) || winsBoard[0].state.equals(State.O) ) { // manual diagonal check
			if (winsBoard[0].equals(winsBoard[4]) && winsBoard[4].equals(winsBoard[8])) {
				if(winsBoard[0].state.equals(State.X))
					winner.state = State.X;
				if(winsBoard[0].state.equals(State.O))
					winner.state = State.O;
			}
		}

		if ( winsBoard[2].state.equals(State.X) || winsBoard[2].state.equals(State.O) ) { // manual diagonal check
			if (winsBoard[2].equals(winsBoard[4]) && winsBoard[4].equals(winsBoard[6])) {
				if(winsBoard[2].state.equals(State.X))
					winner.state = State.X;
				if(winsBoard[2].state.equals(State.O))
					winner.state = State.O;
			}
		}

	}

	/**
	 * Method for setting a piece on the board
	 * @param big the large board it is in
	 * @param little the position on the smaller board
	 * @param val the state to set it to
	 */
	public void setXYXY(int big, int little, State val){
		board[big].board[little].state = val;
		
		/* //Keep this around for save file creation and reverse engineering for loading
		 * int color =  val ? Color.red.getRGB() : Color.blue.getRGB();
		 * bimg.setRGB((big%3)*3 + (little%3), (int)Math.floor(((double)big)/3.0)*3 + (int)Math.floor(((double)little)/3.0), color);
		 */
	}
	
	public void paintComponent(Graphics g){

		//if(winner == null){
		g.setColor(Color.black);
		g.fillRect(0, 0, getWidth(), getHeight());
		g.drawImage(bimg, 0 + buffer, 0, size, size, null);
		for (int big = 0; big < board.length; big++) {
			for (int little = 0; little < board[big].board.length; little++) {

				if(board[big].board[little].state != State.BLANK){

					g.drawImage(board[big].board[little].littleImage(), (big%3)*third + (little%3)*ninth + buffer, 
							(int)Math.floor(((double)big)/3.0)*third + (int)Math.floor(((double)little)/3.0)*ninth,
							ninth, ninth, null);

				}
			}

			if(board[big].winner.state != State.BLANK){
				g.drawImage(board[big].winner.bigImage(), (big%3)*third + buffer, (int)Math.floor(((double)big)/3.0)*third, third, third, null);
			}
		}			
		g.drawImage(lines, 0 + buffer, 0, size, size, null);

		if (!winner.state.equals(State.BLANK)){
			//for (int big = 0; big < board.length; big++) {
			//	if(!board[big].winner.state.equals(State.BLANK)){
			//		g.drawImage(board[big].winner.bigImage(), (big%3)*third + buffer, (int)Math.floor(((double)big)/3.0)*third, third, third, null);
			//	}
			//}
			g.drawImage(lines, 0 + buffer, 0, size, size, null);
			g.setColor(new Color(0, 0, 0, 127));
			g.fillRect(0 + buffer, 0, size, size);
			g.setColor(Color.red);
			String msg = winner.state == State.X ? "X\'s win!" : "O\'s win!";
			Font newfont = new Font(Font.SERIF, Font.BOLD, 72);
			g.setFont(newfont);
			g.drawString(msg, (size/2) - 155 + buffer, size/2);
		}
	}

	/**
	 * The old command line way of displaying the board. 
	 * Probably doesn't work any more
	 */
	@Deprecated
	public void displayBoard(){
		System.out.println(board[0].row1() + "|" + board[1].row1() + "|" + board[2].row1());
		System.out.println(board[0].row2() + "|" + board[1].row2() + "|" + board[2].row2());
		System.out.println(board[0].row3() + "|" + board[1].row3() + "|" + board[2].row3());
		System.out.println("-------+-------+-------");
		System.out.println(board[3].row1() + "|" + board[4].row1() + "|" + board[5].row1());
		System.out.println(board[3].row2() + "|" + board[4].row2() + "|" + board[5].row2());
		System.out.println(board[3].row3() + "|" + board[4].row3() + "|" + board[5].row3());
		System.out.println("-------+-------+-------");
		System.out.println(board[6].row1() + "|" + board[7].row1() + "|" + board[8].row1());
		System.out.println(board[6].row2() + "|" + board[7].row2() + "|" + board[8].row2());
		System.out.println(board[6].row3() + "|" + board[7].row3() + "|" + board[8].row3());
	}

	/**
	 * Override Method for handling mouse inputs.  
	 * Move is counted on release of mouse button
	 */
	private class MouseHandler implements MouseListener{
		@Override public void mouseReleased(MouseEvent e) {
			//If the game hasn't already been won yet
			if(winner.state == State.BLANK){
				//Speed things up and use horizontal buffer to make things accurate
				int x = e.getPoint().y;
				int y = e.getPoint().x - buffer;
				int row = 0;
				int col = 0;
				int big = 0;
				int little = 0;

				if (x < third)  //left column
					col += 0;
				else if (x > 2*third)  // right column
					col += 2;
				else   //center column
					col += 1;

				if (y < third) //top row
					row += 0;
				else if (y > 2*third)  //bottom row
					row += 2;
				else   //center row
					row += 1;

				x -= col*third;
				y -= row*third;

				big = (col*3) + row;


				if(winsBoard[big].state.equals(State.BLANK)){
					System.out.println(board[big].winner.state);
					row = 0;
					col = 0;

					if (x < ninth)  //left column
						col += 0;
					else if (x > 2*ninth)  // right column
						col += 2;
					else   //center column
						col += 1;

					if (y < ninth) //top row
						row += 0;
					else if (y > 2*ninth)  //bottom row
						row += 2;
					else   //center row
						row += 1;

					little = (col*3) + row;

					if(board[big].board[little].state.equals(State.BLANK)){
						if(last == State.X)
							setXYXY(big, little, State.XANIMATION); 
						else 
							setXYXY(big, little, State.OANIMATION);
						System.out.println("Placed " + last + " at " + big + ", " + little);
						last = last == State.X ? State.O : State.X;
					}
				}
			}
		}
		//Stuff I have to include
		@Override public void mouseClicked(MouseEvent e) {}
		@Override public void mousePressed(MouseEvent e) {}
		@Override public void mouseEntered(MouseEvent e) {}
		@Override public void mouseExited(MouseEvent e) {}

	}
}

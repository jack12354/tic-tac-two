package lefthandstudios;

public class Board {
	public Square[] board = new Square[9];
	public Square winner = new Square();
	private Square X = new Square(State.X);
	private Square O = new Square(State.O);

	public Board(){
		System.out.println("init board");
		for (int i = 0; i < board.length; i++) {
			board[i] = new Square();
		}
	}
	//	0 1 2
	//	3 4 5
	//	6 7 8
	public State betterCheckWinner(){
		for (int i = 0; i < board.length; i += 3) {
			if ( board[i].equals(X) || board[i].equals(O) ) { // horizontal check
				if (board[i].equals(board[i+1]) && board[i+1].equals(board[i+2])) {
					if(board[i].equals(X))
						winner = X;
					if(board[i].equals(O))
						winner = O;
					return winner.state;
				}
			}
		}

		for (int i = 0; i < 3; i++) {
			if ( board[i].equals(X) || board[i].equals(O) ) { // vertical check
				if (board[i].equals(board[i+3]) && board[i+3].equals(board[i+6])) {
					if(board[i].equals(X))
						winner = X;
					if(board[i].equals(O))
						winner = O;
					return winner.state;
				}
			}
		}

		if ( board[0].equals(X) || board[0].equals(O) ) { // manual diagonal check
			if (board[0].equals(board[4]) && board[4].equals(board[8])) {
				if(board[0].equals(X))
					winner = X;
				if(board[0].equals(O))
					winner = O;
				return winner.state;
			}
		}

		if ( board[2].equals(X) || board[2].equals(O) ) { // manual diagonal check
			if (board[2].equals(board[4]) && board[4].equals(board[6])) {
				if(board[2].equals(X))
					winner = X;
				if(board[2].equals(O))
					winner = O;
				return winner.state;
			}
		}
		return State.BLANK;
	}
	
	public void setXY(int x, int y, Boolean b){}
	public String row1(){
		return prntln(0);
	}
	public String row2(){
		return prntln(3);
	}
	public String row3(){
		return prntln(6);
	}

	private String prntln(int start){
		String line = " ";
		for(int i = start; i < start+3; i++)
			if(board[i].state != State.BLANK)
				if(board[i].state == State.X)
					line += "X ";
				else
					line += "O ";
			else
				line += "  ";
		return line;
	}
}

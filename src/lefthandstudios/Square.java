package lefthandstudios;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Square {
	static BufferedImage RAWX = new BufferedImage(333, 333, BufferedImage.BITMASK);
	static BufferedImage RAWO = new BufferedImage(333, 333, BufferedImage.BITMASK);
	static BufferedImage BIGX = new BufferedImage(BigBoard.third, BigBoard.third, BufferedImage.BITMASK);
	static BufferedImage BIGO = new BufferedImage(BigBoard.third, BigBoard.third, BufferedImage.BITMASK);
	static BufferedImage SMALLX = new BufferedImage(BigBoard.ninth, BigBoard.ninth, BufferedImage.BITMASK);
	static BufferedImage SMALLO = new BufferedImage(BigBoard.ninth, BigBoard.ninth, BufferedImage.BITMASK);
	static BufferedImage XANIM = new BufferedImage(111, 333, BufferedImage.BITMASK);
	static BufferedImage OANIM = new BufferedImage(111, 333, BufferedImage.BITMASK);
	static BufferedImage animtemp = new BufferedImage(BigBoard.ninth,BigBoard.ninth, BufferedImage.BITMASK);

	public State state = State.BLANK;
	private int animationFrame = -1;

	public Square(){
		state = State.BLANK;
	}

	public Square(State state) {
		this.state = state;
	}

	public static void init(){
		try {			
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			RAWX  = ImageIO.read(classLoader.getResourceAsStream("lefthandstudios/res/BIGX.png"));
			RAWO  = ImageIO.read(classLoader.getResourceAsStream("lefthandstudios/res/BIGO.png"));
			XANIM = ImageIO.read(classLoader.getResourceAsStream("lefthandstudios/res/xsprite.png"));
			OANIM = ImageIO.read(classLoader.getResourceAsStream("lefthandstudios/res/osprite.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		resizeImgs();
	}

	public static void resizeImgs(){
		if(BigBoard.third != BIGX.getHeight()){// avoid resizing to the same size from multiple events fired
			System.out.println("Resizing Images");
			System.out.println(BigBoard.third);
			BIGX = new BufferedImage(BigBoard.third, BigBoard.third, BufferedImage.BITMASK);
			BIGO = new BufferedImage(BigBoard.third, BigBoard.third, BufferedImage.BITMASK);
			SMALLX = new BufferedImage(BigBoard.ninth, BigBoard.ninth, BufferedImage.BITMASK);
			SMALLO = new BufferedImage(BigBoard.ninth, BigBoard.ninth, BufferedImage.BITMASK);

			BIGX.getGraphics().drawImage(RAWX.getScaledInstance(BigBoard.third, BigBoard.third, BufferedImage.SCALE_SMOOTH), 0, 0, BigBoard.third, BigBoard.third, null);
			BIGO.getGraphics().drawImage(RAWO.getScaledInstance(BigBoard.third, BigBoard.third, BufferedImage.SCALE_SMOOTH), 0, 0, BigBoard.third, BigBoard.third, null);
			SMALLX.getGraphics().drawImage(RAWX.getScaledInstance(BigBoard.ninth, BigBoard.ninth, BufferedImage.SCALE_SMOOTH), 0, 0, BigBoard.ninth, BigBoard.ninth, null);
			SMALLO.getGraphics().drawImage(RAWO.getScaledInstance(BigBoard.ninth, BigBoard.ninth, BufferedImage.SCALE_SMOOTH), 0, 0, BigBoard.ninth, BigBoard.ninth, null);
		}
	}

	public Image bigImage(){
		//System.out.println("displaying " + state);
		switch(state){
		case X:
			return BIGX;
		case O:
			return BIGO;
		case XANIMATION:
		case OANIMATION:
			return nextFrame();
		case BLANK:
			return null;
		}
		return null;
	}

	public Image littleImage(){
		//System.out.println("displaying " + state);
		switch(state){
		case X:
			return SMALLX;
		case O:
			return SMALLO;
		case XANIMATION:
		case OANIMATION:
			return nextFrame();
		case BLANK:
			return null;
		}
		return null;
	}

	public Image nextFrame(){
		animationFrame++;
		if(animationFrame*XANIM.getHeight() + 1 > XANIM.getWidth()){
			if(state == State.XANIMATION){
				state = State.X;
				return BIGX;
			} else {
				state = State.O;
				return BIGO;
			}
		}

		if(state == State.XANIMATION)
			return XANIM.getSubimage(111*animationFrame, 0, 111, 111).getScaledInstance(BigBoard.ninth, BigBoard.ninth, BufferedImage.SCALE_SMOOTH);
		if(state == State.OANIMATION)
			return OANIM.getSubimage(111*animationFrame, 0, 111, 111).getScaledInstance(BigBoard.ninth, BigBoard.ninth, BufferedImage.SCALE_SMOOTH);
		return null;
	}

	public boolean equals (Square other){
		if(this.state == State.X || this.state == State.XANIMATION)
			if(other.state == State.X || other.state == State.XANIMATION)
				return true;
		if(this.state == State.O || this.state == State.OANIMATION)
			if(other.state == State.O || other.state == State.OANIMATION)
				return true;
		return this.state == other.state;
	}

}
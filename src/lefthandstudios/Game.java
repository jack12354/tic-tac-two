package lefthandstudios;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Main Entry Point for Tic Tac^2
 * Inspired by Wentworth Game Development Club
 * @author jack
 */
public class Game {
	public static void main(String[] args) {

		//Create default window for game to run inside
		JFrame window = new JFrame();
		window.setTitle("Tic Tac²");
		window.setBackground(Color.black);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setSize(750, 750);
		window.setUndecorated(true);			// Fancy Borderless Mode


		//has to be final for resize to work (is resize needed for mobile port?)
		//final BigBoard bb = new BigBoard(window.getHeight());
		
		//create board and new GamePanel to pack elements into
		BigBoard bb = new BigBoard(window.getHeight());
		GamePanel contentPanel = new GamePanel(new BorderLayout());
		contentPanel.setBackground(Color.BLACK);

	

		//Load game window images
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		ImageIcon titlecard = new ImageIcon();
		ImageIcon newgame = new ImageIcon();
		ImageIcon quit = new ImageIcon();
		try {
			titlecard.setImage(ImageIO.read(classLoader.getResourceAsStream("lefthandstudios/res/titlecard.png")));
			newgame.setImage(ImageIO.read(classLoader.getResourceAsStream("lefthandstudios/res/newgame.png")));
			quit.setImage(ImageIO.read(classLoader.getResourceAsStream("lefthandstudios/res/quit.png")));
		} catch (IOException e) {e.printStackTrace();}

		//Create button subpanel for bottom row of buttons
		JPanel jbutts = new JPanel(new FlowLayout());
		jbutts.setBackground(Color.BLACK);
		
		//Create buttons and add to button subpanel
		JButton jbutt1 = new JButton(newgame);
		jbutt1.setActionCommand("NEW");
		jbutt1.addActionListener(contentPanel);
		jbutt1.setContentAreaFilled(false);
		jbutt1.setBorderPainted(false);
		jbutt1.setFocusable(false);
		JButton jbutt2 = new JButton(quit);
		jbutt2.setActionCommand("QUIT");
		jbutt2.addActionListener(contentPanel);
		jbutt2.setContentAreaFilled(false);
		jbutt2.setBorderPainted(false);
		jbutt2.setFocusable(false);

		jbutts.add(jbutt1);
		jbutts.add(jbutt2);
		contentPanel.add(jbutts, BorderLayout.SOUTH);
		
		//Make title a button as well
		JButton titlebutton = new JButton(titlecard);
		titlebutton.setActionCommand("NEW");
		titlebutton.addActionListener(contentPanel);
		titlebutton.setContentAreaFilled(false);
		titlebutton.setBorderPainted(false);
		titlebutton.setFocusable(false);
		contentPanel.add(titlebutton, BorderLayout.NORTH );		
		
		//add board to main panel and main panel to window
		contentPanel.add(bb, BorderLayout.CENTER);
		window.add(contentPanel);
		window.setVisible(true);
		
		//not really needed, since window.setUndecorated() removes resizing options
		//maybe splash screen with size selection or options button?
//		window.addComponentListener(new ComponentListener() {
//			public void componentResized(ComponentEvent e) {
//				bb.onResize();
//			}
//			public void componentHidden(ComponentEvent arg0) {}
//			public void componentMoved(ComponentEvent arg0) {}
//			public void componentShown(ComponentEvent arg0) {}
//		});
		
		//scale gameboard properly now that it's in the window
		bb.onResize();

		//Continuously check for new winners and repaint at horrendous framerate
		for(;;){
			for (Component comp : contentPanel.getComponents()) {
				if(comp.getClass() == BigBoard.class){
					((BigBoard) comp).checkWinner();
					comp.repaint();
				}
			}
			try{
				Thread.sleep(100);
			} catch (InterruptedException IE){}
		}
	}

	@Deprecated
	public static void newGame(){
		//bb.clearBoard();
		//Add to player scores here
	}

	public static void onResize(){
		System.out.println("pretend this worked");
	}

	@SuppressWarnings("unused")
	private static int convert(int i){
		switch(i){
		case 1: return 6;
		case 2: return 7;
		case 3: return 8;
		case 4: return 3;
		case 5: return 4;
		case 6: return 5;
		case 7: return 0;
		case 8: return 1;
		case 9: return 2;
		default:return 0;
		}
	}
}
